import json
import requests
headers = {"Content-Type": "application/json"}

server="localhost:3001"
urlPassagem = server +"/passagem"
urlPacote= server + "/pacote"
urlHotel = server+"/hotel"



data={'text':'200'}
json_data = json.dumps(data)

#connection.request('GET','/hotel',json_data, headers)
#resposta = connection.getresponse()
#print(resposta.read().decode())

#connection.request('GET','/pacote',json_data, headers)
#resposta = connection.getresponse()
#print(resposta.read().decode())

#connection.request('GET','/passagem',json_data, headers)

#resposta = connection.getresponse()
#print(resposta.read().decode())

def imprimePassagem(passagem):
    print("ID: " + str(passagem['id']))
    print("Origem:" + str(passagem['origem']))
    print("Destino:" + str(passagem['destino']))
    print("Passageiros:" + str( passagem['atualPassageiros']))
    print("Limite de passagens:" + str( passagem['limitePassageiros']))
    print("-=-=-=-=-=-=-=-")

def imprimeHotel(hotel):
    print("Nome do Hotel:" + str(hotel['nome']))
    print("Quantidade de Quartos:" + str(hotel['qtdQuartos']))
    print("Quantidade maxima de pessoas(total):" + str(hotel['qtdPessoas']))
    print("Quantidade Atual de pessoas:" + str(hotel['qtdPessoasAtual']))
    print("Quantidade Atual de Quartos ocupados:" + str(hotel['qtdQuartosOcupados']))
    print("-=-=-=-=-=-=-=-")
    print(" ")

entrada = -1
while entrada != '0':
    print('''
    [1]Listar Passagens
    [2]Reservar Passagens
    [3]Listar Hoteis
    [4]Reservar Hoteis
    [5]Reservar Pacote
    [0]SAIR
    ''')
    entrada=input('Qual das opcoes deseja?')
    if (entrada=='1'):
        # executa o request
        resposta = (requests.get(url='http://localhost:3001/coordenador/passagem/')).json()
        for passagem in resposta['resposta']['dados']:
            imprimePassagem (passagem)

    elif (entrada=='2'):
        origem = input("Origem: ")
        destino = input("Destino: ")
        qtdPessoas = input("Quantidade de pessoas: ")
        dataIda = input("Data de Ida (DD-MM) : ")
        dataVolta = input("Data de Volta (DD-MM): ")
        tipo = input("Tipo (1 - Ida 2 - Ida ou Volta): ")
        ## passo os parametros via body
        r = requests.post(url='http://localhost:3001/coordenador/passagem/reservar/', json={"origem": origem,
                                                                          "destino": destino,
                                                                          "qtdPessoas": qtdPessoas,
                                                                          "dataIda": dataIda,
                                                                          "dataVolta": dataVolta,"tipo" : tipo })
        print(r.json()["dados"]["mensagem"])

    elif (entrada=='3'):
        resposta = (requests.get(url='http://localhost:3001/coordenador/hotel/')).json()
        for hotel in resposta['resposta']['dados']:
            imprimeHotel(hotel)

    elif (entrada=='4'):
        nome = input("Nome do Hotel: ")
        qtdQuartos = input("Quantidade de Quartos: ")
        qtdPessoas = input("Quantidade de Pessoas: ")
        dataEntrada = input("Data da Ida (DD-MM): ")
        dataSaida = input("Data da Volta (DD-MM): ")
        ## passo os parametros via body
        r = requests.post(url='http://localhost:3001/coordenador/hotel/reservar/', json={
                                                                             "nome": nome,
                                                                             "qtdQuartos": qtdQuartos,
                                                                             "qtdPessoas": qtdPessoas,
                                                                             "dataEntrada": dataEntrada,
                                                                             "dataSaida": dataSaida})
        print(r.json()["dados"]["mensagem"])

    elif (entrada=='5'):
        origem = input("Origem: ")
        destino = input("Destino: ")
        nome = input("Nome do Hotel: ")
        qtdQuartos = input("Quantidade de Quartos: ")
        qtdPessoas = input("Quantidade de Pessoas: ")
        dataEntrada = input("Data de Entrada (DD-MM): ")
        dataSaida = input("Data de Saida (DD-MM): ")
        tipo = input("Tipo (1 - Ida 2 - Ida ou Volta): ")
        ## passo os parametros via body
        r = requests.post(url='http://localhost:3001/coordenador/pacote/reservar/', json={
            "origem": origem,
            "destino": destino,
                                                                                 "nome": nome,
                                                                                "qtdQuartos": qtdQuartos,
                                                                                "qtdPessoas": qtdPessoas,
                                                                                "dataEntrada": dataEntrada,
                                                                                "dataSaida": dataSaida,
                                                                                "tipo": tipo})
        print(r.json()["dados"])

    elif (entrada=='0'):
        print("Saindo do sistema")

    else:
        print("Entrada invalida!!")
